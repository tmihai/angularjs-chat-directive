module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    karma: {
	    unit: {
		    configFile: 'karma.conf.js',
		    background: true,
		    singleRun: false
	    }
    },
    sass: {
    	dist: {
			files: {
				'app/style/style.css': 'app/sass/style.scss'
    		}
    	}
    },
    watch: {
    	css: {
			files: '**/*.scss',
			tasks: ['sass']
    	},
		//run unit tests with karma (server needs to be already running)
		karma: {
			files: ['test-main.js', 'app/**/*.js', 'test/**/*.js'],
			tasks: ['karma:unit:run'] //NOTE the :run flag
		}
    }
  });
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['karma:unit:start', 'watch']);
}
