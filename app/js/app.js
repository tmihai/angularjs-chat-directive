define([
    'angular',
    'directives/directives'
], function (ng) {
    'use strict';

    // App module
    return ng.module('app', [ 'directives' ]);
});