define(['../base'], function (directives) {
    'use strict';
    directives.directive('chatConversation', function () {
	    return {
	        restrict: 'E', //E = element, A = attribute, C = class, M = comment         
	        scope: {
	            //@ reads the attribute value, = provides two-way binding, & works with functions
	            user:  '='
	        },
	        templateUrl: 'js/directives/chatconversation/chatconversation.html',
	        controller: 'conversationcontroller',
	        replace: true
	    };
	})
	.controller('conversationcontroller', ['$scope', function($scope) {
		$scope.messages = [
			{
				timestamp: new Date().getTime(),
				user: 'Tiberiu',
				text: 'Hello there...'
			},
			{
				timestamp: new Date().getTime(),
				user: 'Tiberiu',
				text: 'Hello there...'
			}
		];
		
		$scope.whoIsThis = function(userName) {
			return $scope.user === userName ? 'me' : 'other';
		};
		
		$scope.sendMessage = function(event) {
			// on ENTER
			if (event.which === 13) {
				if ($scope.input) {
					var timestamp = new Date().getTime(),
						user      = { displayName: 'Someone' },
						text      = $scope.input;
					var message = {
						timestamp: timestamp,
						user: user.displayName,
						text: text
					};
					$scope.messages.push(message);
					$scope.input = '';
				}
			}
		};
    }]);
});