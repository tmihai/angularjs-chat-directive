var bowerUrl = '../../bower_components/';

require.config({
	
	// urlArgs: "bust=" + (new Date()).getTime(),

	// alias libraries paths
    paths: {
        'domReady': bowerUrl + 'requirejs-domready/domReady',
        'angular':  bowerUrl + 'angular/angular'
    },

    // angular does not support AMD out of the box, put it in a shim
    shim: {
        'angular': {
            exports: 'angular'
        }
    },

    // kick start application
    deps: ['./bootstrap']
});